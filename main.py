from flask import jsonify
from flask import Flask
from flask import request
import host.Host

app = Flask(__name__)

room_list = []

@app.route('/add_room/', methods=['POST'])
def add_host(port):
    content = request.get_json()
    response = {}
    print(request.remote_addr)
    try:
        new_host = Host(request.remote_addr, content["port"], content["login"], content["password"])
        room_list.append(new_host)
    except:
        response["status"] = "Ok"
    else:
        response["status"] = "Error"
    return jsonify(response)

@app.route('/get_rooms', methods=['GET'])
def get_rooms():
    response = {}
    return "HELLO WORLD"

if __name__ == "__main__":
    app.run()
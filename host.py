class Host(object):
    def init(self, ip, port, login, password):
        self.adress = ip
        self.port = port
        self.login = login
        self.password = password
    
    def serialize(self):
        return {
            "adress":self.adress,
            "port":self.port,
            "login":self.login,
            "password":self.password
        }